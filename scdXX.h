﻿/*
 * scdxx.h
 *
 * Created: 31.1.2020 г. 12:22:18
 *  Author: Todor Topalov
 *  Based on Sensirion reference drivers
 */ 


#ifndef SCDXX_H_
#define SCDXX_H_

#include "i2c.h"
#include "util/delay.h"

#define SENSIRION_NUM_WORDS(x) (sizeof(x) / SENSIRION_WORD_SIZE)

class SensirionSensor {
	protected:
	static constexpr uint8_t CRC8_POLYNOMIAL = 0x31;
	static constexpr uint8_t CRC8_INIT = 0xFF;
	static constexpr uint8_t CRC8_LEN = 1;
	static constexpr uint8_t SENSIRION_WORD_SIZE = 2;
	static constexpr uint8_t SENSIRION_COMMAND_SIZE = 2;
	static constexpr uint8_t SENSIRION_MAX_BUFFER_WORDS = 32;

	I2cMaster & i2c;
	uint8_t address;
	bool delayedReadCmd;
	
	SensirionSensor(I2cMaster & i2cMaster, uint8_t addr, bool delayedRead = false)
	: i2c(i2cMaster)
	, address(addr)
	, delayedReadCmd(delayedRead) {}

	static bool checkCrc(uint8_t *data, uint16_t count, uint8_t checksum) {
		return generateCrc(data, count) == checksum;
	}

	static uint8_t generateCrc(uint8_t *data, int count) {
		uint8_t crc = CRC8_INIT;

		/* calculates 8-Bit checksum with given polynomial */
		for (int current_byte = 0; current_byte < count; ++current_byte) {
			crc ^= (data[current_byte]);
			for (uint8_t crc_bit = 8; crc_bit > 0; --crc_bit) {
				if (crc & 0x80)
				crc = (crc << 1) ^ CRC8_POLYNOMIAL;
				else
				crc = (crc << 1);
			}
		}
		return crc;
	}

	static uint16_t fillSendBuffer(uint8_t *buf, uint16_t cmd, const uint16_t *args, uint8_t num_args) {
		uint16_t idx = 0;

		buf[idx++] = (uint8_t)((cmd & 0xFF00) >> 8);
		buf[idx++] = (uint8_t)((cmd & 0x00FF) >> 0);

		for (int i = 0; i < num_args; ++i) {
			buf[idx++] = (uint8_t)((args[i] & 0xFF00) >> 8);
			buf[idx++] = (uint8_t)((args[i] & 0x00FF) >> 0);

			uint8_t crc = generateCrc((uint8_t *)&buf[idx - 2], SENSIRION_WORD_SIZE);
			buf[idx++] = crc;
		}
		return idx;
	}

	bool writeCmd(uint16_t command, const uint16_t *data_words, uint16_t num_words) {
		uint8_t buf[SENSIRION_MAX_BUFFER_WORDS];
		uint16_t buf_size = fillSendBuffer(buf, command, data_words, num_words);
		return i2c.write(address, buf, buf_size);
	}

	bool readCmd(uint16_t cmd, uint16_t *data_words, uint16_t num_words) {
		uint8_t buf[SENSIRION_COMMAND_SIZE];

		fillSendBuffer(buf, cmd, 0, 0);
		_delay_ms(1);
		return i2c.write(address, buf, 2) && readWords(data_words, num_words);
	}

	bool readBytes(uint8_t *data, uint16_t num_words) {
		uint16_t size = num_words * (SENSIRION_WORD_SIZE + CRC8_LEN);
		uint16_t word_buf[SENSIRION_MAX_BUFFER_WORDS];
		uint8_t *const buf8 = (uint8_t *)word_buf;

		if (!i2c.read(address, buf8, size))
		return false;

		/* check the CRC for each word */
		for (uint16_t i = 0, j = 0; i < size; i += SENSIRION_WORD_SIZE + CRC8_LEN) {
			if(!checkCrc(&buf8[i], SENSIRION_WORD_SIZE, buf8[i + SENSIRION_WORD_SIZE]))
			return false;
			data[j++] = buf8[i];
			data[j++] = buf8[i + 1];
		}

		return true;
	}

	bool readWords(uint16_t *data_words, int num_words) {
		if (!readBytes((uint8_t *)data_words, num_words))
		return false;

		for (int i = 0; i < num_words; ++i)
		data_words[i] = switchEndianness(data_words[i]);

		return true;
	}

	static uint16_t switchEndianness(uint16_t reg) {
		return (uint16_t)((reg<<8) | (reg>>8));
	}
};

class SCD30: public SensirionSensor {

public:
	float co2;
	float temperature;
	float humidity;

	SCD30(I2cMaster & i2cMaster)
	: SensirionSensor(i2c, 0xc2) {}

	bool init() {
		uint16_t rdy;
		return getDataReady(&rdy)
			&& setMeasurementInterval(2)
			&& startPeriodicMeasurement(0);
	}

	bool hasData() {
		uint16_t rdy = 0;
		return getDataReady(&rdy) && !!rdy;
	}

	bool read() {
		union {
			uint32_t u32_value;
			float float32;
			uint16_t words[2];
		} tmp, data[3];

		if (!readCmd(0x0300, data->words, SENSIRION_NUM_WORDS(data)))
			return false;

		tmp.u32_value = (data[0].u32_value << 16) | (data[0].u32_value >> 16);
		co2 = tmp.float32;

		tmp.u32_value = (data[1].u32_value << 16) | (data[1].u32_value >> 16);
		temperature = tmp.float32;

		tmp.u32_value = (data[2].u32_value << 16) | (data[2].u32_value >> 16);
		humidity = tmp.float32;

		return true;
	}

private:

	bool getDataReady(uint16_t *data_ready) {
		return readCmd(0x0202, data_ready, SENSIRION_NUM_WORDS(*data_ready));
	}

	bool startPeriodicMeasurement(uint16_t ambient_pressure_mbar) {
		if (ambient_pressure_mbar &&
		(ambient_pressure_mbar < 700 || ambient_pressure_mbar > 1400)) {
			/* out of allowable range */
			return false;
		}

		return writeCmd(0x0010, &ambient_pressure_mbar, SENSIRION_NUM_WORDS(ambient_pressure_mbar));
	}

	bool setMeasurementInterval(uint16_t interval_sec) {
		if (interval_sec < 2 || interval_sec > 1800) {
			/* out of allowable range */
			return false;
		}

		return writeCmd(0x4600, &interval_sec, 1);
	}
};

class SCD40: public SensirionSensor {
public:
	uint16_t co2;
	int32_t temperature;
	int32_t humidity;
	int32_t temperatureOffset;

	SCD40(I2cMaster & i2cMaster)
	: SensirionSensor(i2c, 0xc4, true) {}

	bool init() {
		return startPeriodicMeasurement();
	}

	bool startPeriodicMeasurement() {
		return writeCmd(0x21B1, 0, 0);
	}

	bool hasData() {
		uint16_t rdy = 0;
		return getDataReady(&rdy) && !!rdy;
	}

	bool read() {
		uint16_t data[3];

		if (!readCmd(0xEC05, data, SENSIRION_NUM_WORDS(data)))
			return false;

		co2 = data[0];
		temperature = ((21875 * (int32_t)data[1]) >> 13) - 45000;
		humidity = ((12500 * (int32_t)data[2]) >> 13);

		return true;
	}

	bool readTemperatureOffset() {
		uint16_t data[1];

		if (!readCmd(0x2318, data, SENSIRION_NUM_WORDS(data)))
		return false;

		temperatureOffset = ((21875 * (int32_t)data[0]) >> 13);
	}

	bool stopPeriodicMeasurement() {
		return writeCmd(0x3F86, 0, 0);
	}

	bool setTemperature_offset_ticks(uint16_t offset) {
		uint16_t t_offset = (uint16_t)((offset * 12271) >> 15);
		return writeCmd(0x241D, &t_offset, 1);
	}

	bool getSensorAltitude(uint16_t* sensor_altitude) {
		return readCmd(0x2322, sensor_altitude, 1);
	}

	bool setSensorAltitude(uint16_t sensor_altitude) {
		return writeCmd(0x2427, &sensor_altitude, 1);
	}

	bool setAmbientPressure(uint16_t ambient_pressure) {
		return writeCmd(0xE000, &ambient_pressure, 1);
	}

	bool getAutomaticSelfCalibration(uint16_t* asc_enabled) {
		return readCmd(0x2313, asc_enabled, 1);
	}

	bool setAutomaticSelfCalibration(uint16_t asc_enabled) {
		return writeCmd(0x2416, &asc_enabled, 1);
	}

	bool startLowPowerPeriodicMeasurement() {
		return writeCmd(0x21AC, 0, 0);
	}

	bool getDataReady(uint16_t *data_ready) {
		return readCmd(0xE4B8, data_ready, 1);
	}

	bool persistSettings() {
		return writeCmd(0x3615, 0, 0);
	}

	bool getSerialNumber(uint16_t* serials) {
		return readCmd(0x3682, serials, 3);
	}

	bool performSelfTest(uint16_t* sensor_status) {
		return readCmd(0x3639, sensor_status, 1);
	}

	bool performFactoryReset() {
		return writeCmd(0x3632, 0, 0);
	}

	bool reinit() {
		return writeCmd(0x3646, 0, 0);
	}

	bool powerDown() {
		return writeCmd(0x36E0, 0, 0);
	}

	bool wakeUp() {
		return writeCmd(0x36F6, 0, 0);
	}
};
#endif /* SCDXX_H_ */