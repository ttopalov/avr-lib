#ifndef __LCD_H
#define __LCD_H

/*
NHD-C0220BiZ driver
*/

#include "i2c.h"

class LCD
{
	static const uint8_t ADDRESS = 0x78;
	static const uint8_t COM_SEND = 0x00;
	static const uint8_t DATA_SEND = 0x40;
	static const uint8_t CLEAR_DISPLAY = 0x01;
	static const uint8_t RETURN_HOME = 0x02;
	static const uint8_t LINE_2 = 0xc0;
	static const uint8_t MORE_CONTROL = 0x80;
	static const uint8_t CURSOR_RIGHT = 0x14;
	I2cMaster & i2c;
	char lines[2][20];

	static void copy(void * dest, const void * from, uint8_t cnt) {
		uint8_t * d = reinterpret_cast<uint8_t*>(dest);
		const uint8_t * f = reinterpret_cast<const uint8_t*>(from), * t = f + cnt;
		while (f != t)
			*d++ = *f++;
	}

	bool sendInitSequence() {
		uint8_t buf[] = {COM_SEND, 0x38, 0x39, 0x14, 0x78, 0x5e, 0x6d, 0x0e, 0x06};
		return i2c.write(ADDRESS, buf, sizeof(buf));
	}

	bool refresh(unsigned cursorIndex) {
		if (!sendInitSequence())
			return false;
		uint8_t outBuffer[48] = { COM_SEND | MORE_CONTROL, RETURN_HOME, DATA_SEND };
		copy(outBuffer + 3, lines[0], 20);

		if (!i2c.write(ADDRESS, outBuffer, 23))
			return false;

		outBuffer[1] = LINE_2;
		copy(outBuffer + 3, lines[1], 20);

		if (!i2c.write(ADDRESS, outBuffer, 23))
			return false;

		if (cursorIndex > 40)
			cursorIndex = 40;

		outBuffer[0] = COM_SEND;
		outBuffer[1] = RETURN_HOME;
		outBuffer[2] = 0x38;
		for (unsigned i = 0; i < cursorIndex; i++)
			outBuffer[3 + i] = CURSOR_RIGHT;
		return i2c.write(ADDRESS, outBuffer, 3 + cursorIndex);
	}

	void setLine(int index, const char * from) {
		int zero = !from;
		for (int i = 0; i < 20; i++) {
			zero = zero || !from[i];
			lines[index][i] = zero ? 0 : from[i];
		}
	}

public:
	LCD(I2cMaster & device, const char * line0 = 0, const char * line1 = 0)
		: i2c(device)
	{
		setLine(0, line0);
		setLine(1, line1);
	}

	bool init() {
		return refresh(20);
	}

	bool setText(const char * text, unsigned cursorIndex) {
		copy(lines, text, 40);
		return refresh(cursorIndex);
	}

	bool setLines(const char * line0, const char * line1, unsigned cursorIndex) {
		setLine(0, line0);
		setLine(1, line1);
		return refresh(cursorIndex);
	}
};

#endif //__LCD_H
