# avr-lib

# Various libraries for AVR

Although some have been implemented from scratch, most of them are heavilly modified versions of other open-source projects that I have already lost track of.
That said, I don't necessaryly claim ownership of all the code in here.
Still, of it's useful to me, it could be useful to others.

# How to use

This repository contains only header files. Include it as a submodule in your project and then include the files you need.
In order to compile them, set the standard to c++14 (Microchip Studio supports it).
