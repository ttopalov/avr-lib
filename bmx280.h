﻿/*
 * sensor.h
 *
 * Created: 23.3.2019 г. 14:25:09
 *  Author: todor
 */ 


#ifndef SENSOR_H_
#define SENSOR_H_

#define OVER_0x		0x00 //skipped, output set to 0x80000
#define OVER_1x		0x01
#define OVER_2x		0x02
#define OVER_4x		0x03
#define OVER_8x		0x04
#define OVER_16x	0x05

#define BME280_FORCED_MODE 0x01
#define BME280_NORMAL_MODE 0x03
#define BME280_SLEEP_MODE 0x00

#define BME280_STANDBY_500us	0x00
#define BME280_STANDBY_62500us	0x01
#define BME280_STANDBY_125ms	0x02
#define BME280_STANDBY_250ms	0x03
#define BME280_STANDBY_500ms	0x04
#define BME280_STANDBY_1000ms	0x05
#define BME280_STANDBY_10ms		0x06
#define BME280_STANDBY_20ms		0x07

#define BME280_IIR_OFF	0x00
#define BME280_IIR_2x	0x01
#define BME280_IIR_4x	0x02
#define BME280_IIR_8x	0x03
#define BME280_IIR_16x	0x04

#define BME280_SPI_OFF	0x00
#define BME280_SPI_ON	0x01

//#define BME_ADDR 0xEC
#define BME_ADDR (0x76 << 1)
// for BME280 define
// #define BME280

/****** settings *******/
// default: Standby-Time = 250ms, IIR-Filter = 16x, SPI disable, Oversampling for all Sensors = 16x, Normal Mode

// Standby-Time, IIR-Filter, SPI Disable
#define BME280_CONFIG		(BME280_STANDBY_250ms << 5)|(BME280_IIR_8x << 2)|(BME280_SPI_OFF)
// Temperatur-Sensor
#define BME280_TEMP_CONFIG	OVER_16x
// Pressure-Sensor
#define BME280_PRESS_CONFIG	OVER_16x

#ifdef BME280
// Humitity-Sensor
#define BME280_HUM_CONFIG	OVER_16x
#define BME_CHIP_ID 0x60
#else
#define BME280_HUM_CONFIG	OVER_0x	//no humitity-sensor, skipped
#define BME_CHIP_ID 0x58
#endif
// Mode
#define BME280_MODE_CONFIG	BME280_NORMAL_MODE

#include "i2c.h"

struct bme280_calib_data
{
	uint16_t dig_T1;	// 0x88
	int16_t  dig_T2;	// 0x8A
	int16_t  dig_T3;	// 0x8C
	
	uint16_t dig_P1;	// 0x8E
	int16_t  dig_P2;	// 0x90
	int16_t  dig_P3;	// 0x92
	int16_t  dig_P4;	// 0x94
	int16_t  dig_P5;	// 0x96
	int16_t  dig_P6;	// 0x98
	int16_t  dig_P7;	// 0x9A
	int16_t  dig_P8;	// 0x9C
	int16_t  dig_P9;	// 0x9E
	#ifdef BME280
	uint8_t  dig_H1;	// 0xA1
	int16_t  dig_H2;	// 0xE1
	uint8_t  dig_H3;	// 0xE3
	int16_t  dig_H4;	// 0xE4
	int16_t  dig_H5;	// 0xE5
	int8_t   dig_H6;	// 0xE7
	#endif
};

enum
{
	BME280_REGISTER_DIG_T1              = 0x88,
	BME280_REGISTER_DIG_T2              = 0x8A,
	BME280_REGISTER_DIG_T3              = 0x8C,
	
	BME280_REGISTER_DIG_P1              = 0x8E,
	BME280_REGISTER_DIG_P2              = 0x90,
	BME280_REGISTER_DIG_P3              = 0x92,
	BME280_REGISTER_DIG_P4              = 0x94,
	BME280_REGISTER_DIG_P5              = 0x96,
	BME280_REGISTER_DIG_P6              = 0x98,
	BME280_REGISTER_DIG_P7              = 0x9A,
	BME280_REGISTER_DIG_P8              = 0x9C,
	BME280_REGISTER_DIG_P9              = 0x9E,
	#ifdef BME280
	BME280_REGISTER_DIG_H1              = 0xA1,
	BME280_REGISTER_DIG_H2              = 0xE1,
	BME280_REGISTER_DIG_H3              = 0xE3,
	BME280_REGISTER_DIG_H4              = 0xE4,
	BME280_REGISTER_DIG_H5              = 0xE5,
	BME280_REGISTER_DIG_H6              = 0xE7,
	#endif
	BME280_REGISTER_CHIPID             = 0xD0,
	BME280_REGISTER_VERSION            = 0xD1,
	BME280_REGISTER_SOFTRESET          = 0xE0,
	
	BME280_REGISTER_CAL26              = 0xE1,  // R calibration stored in 0xE1-0xF0
	
	BME280_REGISTER_CONTROLHUMID       = 0xF2,
	BME280_REGISTER_CONTROL            = 0xF4,
	BME280_REGISTER_CONFIG             = 0xF5,
	BME280_REGISTER_PRESSUREDATA       = 0xF7,
	BME280_REGISTER_TEMPDATA           = 0xFA,
	#ifdef BME280
	BME280_REGISTER_HUMIDDATA          = 0xFD,
	#endif
};

class BME280Sensor {

	I2cMaster & i2c;
	int32_t t_fine;
	bme280_calib_data _bme280_calib;
	bool initialized;
	bool calibrated;

	bool checkInit() {
		initialized = initialized || init();
		return initialized;
	}

	uint8_t read1Byte(uint8_t addr) {
		uint8_t value;
		i2c.readFromAddress(BME_ADDR, addr, &value, 1);
		return value;
	}
	uint16_t read2Byte(uint8_t addr) {
		uint8_t buffer[2];
		i2c.readFromAddress(BME_ADDR, addr, buffer, 2);
		uint16_t value;
		value = buffer[0];
		value <<= 8;
		value |= buffer[1];
		return value;
	}
	uint32_t read3Byte(uint8_t addr) {
		uint8_t buffer[3];
		i2c.readFromAddress(BME_ADDR, addr, buffer, 3);
		uint32_t value;
		value = buffer[0];
		value <<= 8;
		value |= buffer[1];
		value <<= 8;
		value |= buffer[2];
		return value;
	}
	uint16_t read16_LE(uint8_t reg) {
		uint16_t temp = read2Byte(reg);
		return (temp >> 8) | (temp << 8);
	}

	int16_t readS16(uint8_t reg) {
		return (int16_t)read2Byte(reg);
	}

	int16_t readS16_LE(uint8_t reg) {
		return (int16_t)read16_LE(reg);
	}

	bool readCoefficients(void) {
		bool success = i2c.readFromAddress(BME_ADDR, BME280_REGISTER_DIG_T1, (uint8_t*)&_bme280_calib, 12 * 2);
		#ifdef BME280
		_bme280_calib.dig_H1 = read1Byte(BME280_REGISTER_DIG_H1);
		_bme280_calib.dig_H2 = readS16_LE(BME280_REGISTER_DIG_H2);
		_bme280_calib.dig_H3 = read1Byte(BME280_REGISTER_DIG_H3);
		_bme280_calib.dig_H4 = (read1Byte(BME280_REGISTER_DIG_H4) << 4) | (read1Byte(BME280_REGISTER_DIG_H4+1) & 0xF);
		_bme280_calib.dig_H5 = (read1Byte(BME280_REGISTER_DIG_H5+1) << 4) | (read1Byte(BME280_REGISTER_DIG_H5) >> 4);
		_bme280_calib.dig_H6 = (int8_t)read1Byte(BME280_REGISTER_DIG_H6);
		#endif
		return success;
	}

	bool readTemperature(void) {
		uint32_t adc_T = read3Byte(BME280_REGISTER_TEMPDATA);
		if (i2c.I2C_ErrorCode || adc_T == 0x800000) { // value in case pressure measurement was disabled
			temperature = 0x8000;
			return false;
		}
		int32_t var1, var2;
			
		adc_T >>= 4;
			
		var1  = ((((adc_T>>3) - ((int32_t)_bme280_calib.dig_T1 <<1))) *
		((int32_t)_bme280_calib.dig_T2)) >> 11;
			
		var2  = (((((adc_T>>4) - ((int32_t)_bme280_calib.dig_T1)) *
		((adc_T>>4) - ((int32_t)_bme280_calib.dig_T1))) >> 12) *
		((int32_t)_bme280_calib.dig_T3)) >> 14;
			
		t_fine = var1 + var2;
			
		uint32_t T  = (t_fine * 5 + 128) >> 8;
			
		temperature = (T + 50) / 100;
		return true;
	}

	bool readPressure(void) {
		int64_t var1, var2, p;
			
		int32_t adc_P = read3Byte(BME280_REGISTER_PRESSUREDATA);
		if (i2c.I2C_ErrorCode || adc_P == 0x800000) { // value in case pressure measurement was disabled
			pressure = -1;
			return false;
		}
		adc_P >>= 4;
			
		var1 = ((int64_t)t_fine) - 128000ul;
		var2 = var1 * var1 * (int64_t)_bme280_calib.dig_P6;
		var2 = var2 + ((var1*(int64_t)_bme280_calib.dig_P5)<<17);
		var2 = var2 + (((int64_t)_bme280_calib.dig_P4)<<35);
		var1 = ((var1 * var1 * (int64_t)_bme280_calib.dig_P3)>>8) +
		((var1 * (int64_t)_bme280_calib.dig_P2)<<12);
		var1 = (((((int64_t)1)<<47)+var1))*((int64_t)_bme280_calib.dig_P1)>>33;
			
		if (var1 == 0) {
			pressure = 0; // avoid exception caused by division by zero
			return false;
		}
		p = 1048576ul - adc_P;
		p = (((p<<31) - var2)*3125ul) / var1;
		var1 = (((int64_t)_bme280_calib.dig_P9) * (p>>13) * (p>>13)) >> 25;
		var2 = (((int64_t)_bme280_calib.dig_P8) * p) >> 19;

		p = ((p + var1 + var2) >> 8) + (((int64_t)_bme280_calib.dig_P7)<<4);
		pressure = (p + 127)/256ul / 100;
		return true;
	}
#ifdef BME280
	bool readHumidity(void) {
		int32_t adc_H = read2Byte(BME280_REGISTER_HUMIDDATA);
		if (i2c.I2C_ErrorCode || adc_H == 0x8000) { // value in case humidity measurement was disabled
			humidity = -1;
			return false;
		}
		int32_t v_x1_u32r;
			
		v_x1_u32r = (t_fine - ((int32_t)76800));
			
		v_x1_u32r = (((((adc_H << 14) - (((int32_t)_bme280_calib.dig_H4) << 20) -
		(((int32_t)_bme280_calib.dig_H5) * v_x1_u32r)) + ((int32_t)16384)) >> 15) *
		(((((((v_x1_u32r * ((int32_t)_bme280_calib.dig_H6)) >> 10) *
		(((v_x1_u32r * ((int32_t)_bme280_calib.dig_H3)) >> 11) + ((int32_t)32768))) >> 10) +
		((int32_t)2097152)) * ((int32_t)_bme280_calib.dig_H2) + 8192) >> 14));
			
		v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) *
		((int32_t)_bme280_calib.dig_H1)) >> 4));
			
		v_x1_u32r = (v_x1_u32r < 0) ? 0 : v_x1_u32r;
		v_x1_u32r = (v_x1_u32r > 419430400) ? 419430400 : v_x1_u32r;
		uint32_t h = (v_x1_u32r>>12);
		humidity = (h + 512) / 1024;
		return true;
	}
#endif

public:
	int16_t temperature;
	int16_t pressure;
	int16_t humidity;

	BME280Sensor(I2cMaster & i2cDevice)
		: i2c(i2cDevice)
		, t_fine(0)
		, initialized(false)
		, calibrated(false)
		, temperature(0)
		, pressure(0)
		, humidity(0)
		{}
	bool init(void) {
		if (read1Byte(BME280_REGISTER_CHIPID) != BME_CHIP_ID)
			return false;
			// configure sensor
#ifdef BME280
		uint8_t confData[] = {
			BME280_REGISTER_CONTROLHUMID, BME280_HUM_CONFIG,
			BME280_REGISTER_CONFIG, BME280_CONFIG,
			BME280_REGISTER_CONTROL, (BME280_TEMP_CONFIG << 5)|(BME280_PRESS_CONFIG << 2)|(BME280_MODE_CONFIG)
		};
#else
		uint8_t confData[] = {
			BME280_REGISTER_CONFIG, BME280_CONFIG,
			BME280_REGISTER_CONTROL, (BME280_TEMP_CONFIG << 5)|(BME280_PRESS_CONFIG << 2)|(BME280_MODE_CONFIG)
		};
#endif
		if (!i2c.write(BME_ADDR, confData, sizeof(confData)))
			return false;

		calibrated = calibrated || readCoefficients();
		return calibrated;
	}

	bool read() {
		bool success = checkInit();
		success = success && readTemperature();
		success = success && readPressure();
#ifdef BME280
		success = success && readHumidity();
#endif
		initialized = success;
		return success;
	}
};

#endif /* SENSOR_H_ */