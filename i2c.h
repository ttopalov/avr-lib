

#ifndef i2c_h
#define i2c_h

#include <avr/io.h>
#include <util/twi.h>
#include <util/delay.h>

#ifndef TWSR
#define TWSR TWSR0
#define TWBR TWBR0
#define TWCR TWCR0
#define TWDR TWDR0
#endif

#ifndef F_I2C
#define F_I2C			10000UL// clock i2c
#endif
#ifndef PSC_I2C
#define PSC_I2C			4		// prescaler i2c
#endif
#define SET_TWBR		(F_CPU / F_I2C-16UL) / (PSC_I2C * 2UL)

static_assert(PSC_I2C == 1 || PSC_I2C == 4 || PSC_I2C == 16 || PSC_I2C == 64, "Wrong prescaler for TWI !");
static_assert(SET_TWBR >= 0 && SET_TWBR <= 255, "TWBR out of range, change PSC_I2C or F_I2C !");

class I2cMaster {
	static constexpr uint8_t I2C_START = 0;
	static constexpr uint8_t I2C_SENDADRESS = 1;
	static constexpr uint8_t I2C_BYTE = 2;
	static constexpr uint8_t I2C_READACK = 3;
	static constexpr uint8_t I2C_READNACK = 4;
	static constexpr uint16_t TIMEOUT = F_CPU / F_I2C * 2.0;

	bool await(uint8_t flag, uint8_t expectedStatus, uint8_t error_code) {
		uint16_t timeout = TIMEOUT;
		while(!(TWCR & flag) && --timeout);
		if ((TWSR & 0xF8) != expectedStatus) {
			I2C_ErrorCode |= error_code;
			return false;
		}
		return true;
	}

	void start(uint8_t i2c_addr) {
		TWCR = 0;
		// i2c start
		TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
		if (!await(1 << TWINT, TW_START, 1 << I2C_START))
			return;
		// send adress
		TWDR = i2c_addr;
		TWCR = (1 << TWINT) | (1 << TWEN);
		await(1 << TWINT, (i2c_addr & 1) ? TW_MR_SLA_ACK : TW_MT_SLA_ACK, 1 << I2C_SENDADRESS);
	}

	void stop(void) {
		TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
	}

	void byte(uint8_t byte) {
		TWDR = byte;
		TWCR = (1 << TWINT) | (1 << TWEN);
		await(1 << TWINT, TW_MT_DATA_ACK , 1 << I2C_BYTE);
	}

	uint8_t readAck(void) {
		TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
		await(1 << TWINT, TW_MR_DATA_ACK, 1 << I2C_READACK);
		return TWDR;
	}

	uint8_t readNAck(void) {
		TWCR = (1<<TWINT) | (1<<TWEN);
		await(1 << TWINT, TW_MR_DATA_NACK, 1 << I2C_READNACK);
		return TWDR;
	}

public:
	uint8_t I2C_ErrorCode;

	void init() {
		// set clock
		switch (PSC_I2C) {
		case 4:
			TWSR = 0x1;
			break;
		case 16:
			TWSR = 0x2;
			break;
		case 64:
			TWSR = 0x3;
			break;
		default:
			TWSR = 0x00;
			break;
		}
		TWBR = (uint8_t)SET_TWBR;
	}

	void reset() {
		TWCR  = 0;
		_delay_us(100);
	}

	bool write(uint8_t addr, const uint8_t * toWrite, uint8_t writeLen) {
		I2C_ErrorCode = 0;
		start(addr);
		for (uint8_t i = 0; i < writeLen && !I2C_ErrorCode; i++) {
			byte(toWrite[i]);
		}
		stop();
		return !I2C_ErrorCode;
	}

	bool read(uint8_t addr, uint8_t * buffer, uint8_t readLen) {
		I2C_ErrorCode = 0;
		start(addr | 0x01);
		for (uint8_t i = 0; i < readLen && !I2C_ErrorCode; i++)
			buffer[i] = i + 1 < readLen ? readAck() : readNAck();
		stop();
		return !I2C_ErrorCode;
	}

	bool readFromAddress(uint8_t i2cAddress, uint8_t startAddr, uint8_t * buffer, uint8_t readLen) {
		return write(i2cAddress, &startAddr, 1)
			&& read(i2cAddress, buffer, readLen);
	}
};

#endif /* i2c_h */