/*
 * Modbus.h
 *
 * Created: 2.5.2018 г. 11:00:23
 *  Author: Todor Topalov
 */ 


#ifndef MODBUS_H_
#define MODBUS_H_

#include <util/crc16.h>
#include "util.h"

#ifndef UCSRC
#define UCSRC UCSR0C
#define UCSRB UCSR0B
#define UCSRA UCSR0A
#define UDRIE UDRIE0
#define TCCR2 TCCR2B
#define OCR2 OCR2A
#define UBRRL UBRR0L
#define UBRRH UBRR0H
#define RXEN RXEN0
#define TXEN TXEN0
#define RXCIE RXCIE0
#define TXC TXC0
#define RXC RXC0
#define UDR UDR0
#define UDRE UDRE0
#define UCSZ0 UCSZ00
#define URSEL 8
#define USBS USBS0
#define TIFR TIFR2
#define OCF2 OCF2A
#endif

struct DefMbHwSettings {
	static constexpr uint8_t OUT_DE = (1 << 2);
	static constexpr bool interruptDriven = true;
	static constexpr auto DE_PORT = &PORTD;
	static constexpr auto DE_DDR = &DDRD;
};

template<typename S = DefMbHwSettings, typename CounterType = uint8_t>
class ModbusSlave {
	static constexpr CounterType MAX_MESSAGE_SIZE = 255;
	static constexpr CounterType MAX_REGISTER_SEND_COUNT = 125;
	static constexpr uint8_t READ_COILS = 0x01;
	static constexpr uint8_t READ_DISCRETE_INPUTS = 0x02;
	static constexpr uint8_t READ_INPUT_REGISTERS = 0x04;
	static constexpr uint8_t READ_HOLDING_REGISTERS = 0x03;
	static constexpr uint8_t WRITE_MULTIPLE_COILS = 0x0f;
	static constexpr uint8_t WRITE_MULTIPLE_REGISTERS = 0x10;

	volatile uint8_t address;
	volatile CounterType inCnt;
	volatile CounterType outCnt;
	volatile CounterType outIndex;
	volatile bool sending;
	volatile uint8_t sendTimeout;
	volatile uint8_t timeout;
	const int16_t * const inputRegisters;
	int16_t * const holdingRegisters;
	uint8_t * const coils;
	uint8_t * const inputs;
	const uint16_t inputRegisterCount;
	const uint16_t holdingRegisterCount;
	const uint16_t coilbyteCount;
	const uint16_t inputbyteCount;
	uint8_t TIMEOUT;
	uint8_t inBuf[MAX_MESSAGE_SIZE];
	uint8_t outBuf[MAX_MESSAGE_SIZE];
	
	void sendPacket(uint8_t len) {
		if (!len)
			return;
		uint16_t crc = CRC16(outBuf, len);
		outBuf[len++] = crc & 0xff;
		outBuf[len++] = crc >> 8;
		outCnt = len;
		outIndex = 0;
		if (S::interruptDriven) {
			UCSRB |= (1 << UDRIE);
		}
	}

	void packetReceived() {
		uint8_t code = inBuf[1];
		outBuf[0] = address;
		outBuf[1] = code;
		uint8_t len = 2;
		switch(code) {
		case READ_INPUT_REGISTERS:
		case READ_HOLDING_REGISTERS: {
			uint16_t count = (inBuf[4] << 8) | inBuf[5];
			uint16_t offset = (inBuf[2] << 8) | inBuf[3];
			uint16_t available = code == READ_INPUT_REGISTERS ? inputRegisterCount : holdingRegisterCount;
			const int16_t * regs = code == READ_INPUT_REGISTERS ? inputRegisters : holdingRegisters;
			if(offset + count <= available && count <= MAX_REGISTER_SEND_COUNT) {
				outBuf[2] = 2 * count;	//2*N bytes
				len = 3;
				for(uint8_t i = offset; i < offset + count; i++) {
					outBuf[len++] = regs[i] >> 8;
					outBuf[len++] = regs[i] & 0xff;
				}
			} else {
				outBuf[1] |= 0x80;
				outBuf[2] = 2;
				len = 3;
			}
			break;
		}
		case WRITE_MULTIPLE_REGISTERS: {
			uint16_t count = (inBuf[4] << 8) | inBuf[5];
			uint16_t offset = (inBuf[2] << 8) | inBuf[3];
			if(offset + count <= holdingRegisterCount) {
				outBuf[2] = (offset >> 8);
				outBuf[3] = (offset & 0xff);
				outBuf[4] = (count >> 8);
				outBuf[5] = (count & 0xff);
				len = 6;

				uint8_t * bytes = inBuf + 8;
				for(uint8_t i = offset; i < offset + count; i++) {
					uint8_t hi = *bytes++;
					uint8_t lo = *bytes++;
					holdingRegisters[i] = (hi << 8) | lo;
				}
			} else {
				outBuf[1] |= 0x80;
				outBuf[2] = 2;
				len = 3;
			}
			break;
		}
		case READ_COILS:
		case READ_DISCRETE_INPUTS:	{
			uint16_t bitCount = (inBuf[4] << 8) | inBuf[5];
			uint16_t bitOffset = (inBuf[2] << 8) | inBuf[3];
			uint16_t available = code == READ_COILS ? coilbyteCount : inputbyteCount;
			uint8_t * bytes = code == READ_COILS ? coils : inputs;
			if(bitOffset + bitCount <= available * 8 && bitCount <= MAX_REGISTER_SEND_COUNT * 16) {
				uint8_t byteCount = (bitCount + 7) / 8;
				outBuf[2] = byteCount;
				len = 3;
				copyBits(bytes, outBuf + len, bitCount, bitOffset, 0);
				len += byteCount;
			} else {
				outBuf[1] |= 0x80;
				outBuf[2] = 2;
				len = 3;
			}
			break;
		}
		case WRITE_MULTIPLE_COILS: {
			uint8_t bitCount = (inBuf[4] << 8) | inBuf[5];
			uint8_t bitOffset = (inBuf[2] << 8) | inBuf[3];
			uint8_t byteCount = inBuf[6];
			if(bitCount + bitOffset <= coilbyteCount * 8 && bitCount < byteCount * 8) {
				len = 6;
				for(uint8_t i = 2; i < 6; i++) {
					outBuf[i] = inBuf[i];
				}
				copyBits(inBuf + 7, coils, bitCount, 0, bitOffset);
			} else {
				outBuf[1] |= 0x80;
				outBuf[2] = 2;
				len = 3;
			}
			break;
		}
		default:
			outBuf[1] |= 0x80;
			outBuf[2] = 1;
			len = 3;
			break;
		}
		
		sendPacket(len);
	}

	static void initTimer() {
		TCCR2 = (1 << WGM21) | (1 << CS22) | (1 << CS21);   // mode CTC : Clear Timer on Compare,  f_quartz = 8 MHz / 256 = 32 kHz
		OCR2 = 8;          // one tick every 0.25 ms
	}

public:
	enum ModbusSpeed { S_4800 = 3, S_9600 = 2, S_19200 = 1, S_38400 = 0 };

	void reconfigure(uint8_t speed, uint8_t addr) {
		address = addr;
		uint8_t prescalers[] = { 12, 25, 51, 103 };
		uint8_t timeouts[] = { 16, 8, 4, 2 };
		TIMEOUT = timeouts[speed];
		if (UBRRL != prescalers[speed]) {
			UBRRH = 0;
			UBRRL = prescalers[speed];
		}
	}

	ModbusSlave(const int16_t * inRegs, uint16_t inRegCount, int16_t * holdRegs, uint16_t holdRegCount, uint8_t * pCoils, uint16_t coilCnt, uint8_t * pIns, uint16_t inCount)
		: address(1)
		, inCnt(0)
		, outCnt(0)
		, outIndex(0)
		, sending(0)
		, sendTimeout(0)
		, timeout(0)
		, inputRegisters(inRegs)
		, holdingRegisters(holdRegs)
		, coils(pCoils)
		, inputs(pIns)
		, inputRegisterCount(inRegCount)
		, holdingRegisterCount(holdRegCount)
		, coilbyteCount(coilCnt)
		, inputbyteCount(inCount)
	{
		/* Enable receiver and transmitter */
		UCSRB = S::interruptDriven ?
			(1 << RXEN) | (1 << TXEN) | (1<<RXCIE) : (1 << RXEN) | (1 << TXEN);
		/* Set frame format: 8data, 2stop bit */
		UCSRC = uint8_t(1 << URSEL) | (1 << USBS) | (3 << UCSZ0);

		*S::DE_DDR |= S::OUT_DE;
		reconfigure(S_19200, 1);
		initTimer();
	}

	void onReceive(uint8_t rec) {
		if (!sending) {
			timeout = TIMEOUT;
			if(inCnt < MAX_MESSAGE_SIZE) {
				inBuf[inCnt++] = rec;
			}
		}
	}

	void onDataRegEmpty() {
		if(outCnt) {
			sending = true;
			*S::DE_PORT |= S::OUT_DE;
			UCSRA |= (1 << TXC);
			UDR = outBuf[outIndex];
			++outIndex;
			--outCnt;
			sendTimeout = 255;
		} else if (UCSRA & (1 << TXC)) {
			*S::DE_PORT &= (~S::OUT_DE);
			UCSRA |= (1 << TXC);
			sending = false;
			UCSRB &= ~(1 << UDRIE);
			sendTimeout = 255;
		}
	}

	void doRs() {
		if (!S::interruptDriven) {
			if (UCSRA & (1<<RXC)) {
				onReceive(UDR);
			}
			if (UCSRA & (1<<UDRE)) {
				onDataRegEmpty();
			}
		}
		if (TIFR & (1 << OCF2)) {	//every 0.25ms
			TIFR = (1 << OCF2);
			if (timeout) {
				if (--timeout == 0) {	//is it a message...is it for me
					if (inBuf[0] == address && inCnt > 3) {	//it is for me then!
						if(CRC16(inBuf, inCnt - 2) == *reinterpret_cast<uint16_t*>(inBuf + inCnt - 2))	//checksum OK
							packetReceived();
					}
					inCnt = 0;
				}
			}
			if (sendTimeout)
				sendTimeout--;
			if ((*S::DE_PORT & S::OUT_DE) && !sendTimeout) {	//something went very wrong with UART interrupts. Abort!!!
				UCSRB &= ~(1 << UDRIE);
				UCSRA |= (1 << TXC);
				sending = false;
				outCnt = 0;
				*S::DE_PORT &= (~S::OUT_DE);
			}
		}
	}
};

#endif /* MODBUS_H_ */