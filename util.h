﻿/*
 * util.h
 *
 * Created: 29.3.2020 г. 22:03:21
 *  Author: Todor Topalov
 */ 


#ifndef UTIL_H_
#define UTIL_H_

#include <avr/eeprom.h>

inline unsigned short CRC16(void * msg, unsigned char len) {
	unsigned short crc = 0xffff;
	uint8_t * _msg = static_cast<uint8_t*>(msg);
	for (int i = 0; i < len; i++)
	crc = _crc16_update(crc, _msg[i]);
	return crc;
}

inline void copyBits(const uint8_t* from, uint8_t* to, uint8_t length, uint8_t fromOffset, uint8_t toOffset) {
	while (length--) {
		from += fromOffset / 8;
		fromOffset %= 8;
		to += toOffset / 8;
		toOffset %= 8;
		*to &= ~(1 << toOffset);
		*to |= (((*from >> fromOffset) & 1) << toOffset);
		fromOffset++;
		toOffset++;
	}
}

inline void numberToString(int16_t number, char * from, char * to) {
	bool negative = number < 0;
	if (negative)
		number = -number;
	do {
		*--to = '0' + number % 10;
		number /= 10;
	} while (to > from && number);

	if (negative && to > from)
		*--to = '-';
}

inline uint8_t getDigitCount(int16_t number) {
	uint8_t count = number < 0 ? 1 : 0;
	do {
		number /= 10;
		count++;
	} while (number);
	return count;
}

inline void printLeftAllign(int16_t number, char * from, char * limit) {
	char * to = from + getDigitCount(number);
	if (to > limit)
		to = limit;
	numberToString(number, from, to);
}

inline void printRightAllign(int16_t number, char * limit, char * to) {
	char * from = to - getDigitCount(number);
	if (from < limit)
		from = limit;
	numberToString(number, from, to);
}

template<typename T>
class SettingsStore {
public:
	SettingsStore() {
		eeprom_read_block(this, 0, sizeof(SettingsStore));
		if (CRC16(this, sizeof(SettingsStore)) != eeprom_read_word((uint16_t*)sizeof(SettingsStore))) {
			settings.setDefaults();
		}
	}

	T settings;

	void save() {
		eeprom_update_block(this, 0, sizeof(SettingsStore));
		eeprom_update_word((uint16_t*)sizeof(SettingsStore), CRC16(this, sizeof(SettingsStore)));
	}
};

#endif /* UTIL_H_ */