/*
 * io.h
 *
 *  Author: Todor Topalov
 */ 


#ifndef __IO_H_
#define __IO_H_

class ButtonHandler {
	uint8_t btnStates = 0;
	uint8_t btnTimes[2] = { 0 };
	const uint8_t mask;
	const uint8_t lpMask;

public:
	ButtonHandler(uint8_t buttonMask, uint8_t longPressMask)
		: mask(buttonMask)
		, lpMask(longPressMask)
		{}

	uint8_t updateClicks(uint8_t ioState) {
		ioState = (ioState & mask & ~lpMask) | updateLpStates(ioState);
		uint8_t prevState = btnStates;
		btnStates = ioState;
		return ~prevState & mask & btnStates;
	}

	uint8_t updateLpStates(uint8_t ioState) {
		btnTimes[0] &= ioState;
		btnTimes[1] &= ioState;
		return btnTimes[1];
	}

	void onTimer(uint8_t ioState) {
		btnTimes[1] = btnTimes[0] & ioState;
		btnTimes[0] = ioState & lpMask;
	}
};

#endif